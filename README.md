## Introduction

This project shows an example of how to make a good database query using filters. It also shows how a project should be structured to separate the persistence, domain and application layers.

The idea of this API REST application is answer price requests to a database by applying appropriate filters to obtain the best response value.

## Technologies

Project is created with:
* Java as programming language 
* Spring Boot
* Maven
* H2 for database
* JUnit for testing

## Setup

To run this project, first clone the url using `git clone`. When you have cloned the project 
you can package it and run the tests with the command `mvn clean package` 

If you wish to start up the project in order to make your queries use the command `mvn spring-boot:run`

## API Reference 

Now, you are able to make a GET request to the API with an url structure as follows: 

`localhost:8080/prices?date=2020-06-15-10.00.00&productId=35455&brandId=1`. 

This url has three required parameters:

* `date` This parameter is a LocalDateTime with the following format `yyyy-MM-dd-HH.mm.ss` 
* `productId` This parameter is a String that only accept Integer numbers 
* `brandId` This parameter is a String that only accept Integer numbers

This request will provide a JSON response with a data structure similar to the following:

``` json
{
    "productId": 35455,
    "brandId": 1,
    "priceList": 3,
    "startDate": "2020-06-15-00.00.00",
    "endDate": "2020-06-15-11.00.00",
    "price": "30.50 EUR"
}
```


