package practice.v1.application.usecases;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;

import java.util.ArrayList;
import java.util.List;

import practice.v1.application.model.SearchPriceRequest;
import practice.v1.domain.aggregates.Price;
import practice.v1.domain.exceptions.PriceNotFoundException;
import practice.v1.domain.repositories.PriceRepository;
import practice.v1.domain.services.PriceService;
import practice.v1.domain.util.FormatUtil;
import practice.v1.domain.vos.Amount;
import practice.v1.domain.vos.Currency;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


class SearchPriceUseCaseTest {

    private final PriceRepository priceRepository = Mockito.mock(PriceRepository.class);
    private final PriceService priceService = new PriceService(priceRepository);
    private final SearchPriceUseCase useCase = new SearchPriceUseCase(priceService);

    @Test
    public void when_productId_35435_brandId_1_date_20200614_100000_then_price_35_50() {

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-14-10.00.00"))
                .productId(35455)
                .brandId(1)
                .build();

        Price price = useCase.execute(request);


        assertEquals(35455, price.getProductId());
        assertEquals(1, price.getBrandId());
        assertEquals("2020-06-14-00.00.00", FormatUtil.toFormat(price.getStartDate()));
        assertEquals("2020-12-31-23.59.59", FormatUtil.toFormat(price.getEndDate()));
        assertEquals(1, price.getPriceList());
        assertEquals("35.50 EUR", price.getFinalProduct());

    }


    @Test
    public void when_productId_35435_brandId_1_date_20200614_160000_then_price_25_45(){

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-14-16.00.00"))
                .productId(35455)
                .brandId(1)
                .build();

        Price price = useCase.execute(request);


        assertEquals(35455, price.getProductId());
        assertEquals(1, price.getBrandId());
        assertEquals("2020-06-14-15.00.00", FormatUtil.toFormat(price.getStartDate()));
        assertEquals("2020-06-14-18.30.00", FormatUtil.toFormat(price.getEndDate()));
        assertEquals(2, price.getPriceList());
        assertEquals("25.45 EUR", price.getFinalProduct());

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200614_210000_then_price_35_50() {

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-14-21.00.00"))
                .productId(35455)
                .brandId(1)
                .build();

        Price price = useCase.execute(request);

        assertEquals(35455, price.getProductId());
        assertEquals(1, price.getBrandId());
        assertEquals("2020-06-14-00.00.00", FormatUtil.toFormat(price.getStartDate()));
        assertEquals("2020-12-31-23.59.59", FormatUtil.toFormat(price.getEndDate()));
        assertEquals(1, price.getPriceList());
        assertEquals("35.50 EUR", price.getFinalProduct());

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200616_210000_then_price_38_95() {

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-16-21.00.00"))
                .productId(35455)
                .brandId(1)
                .build();

        Price price = useCase.execute(request);

        assertEquals(35455, price.getProductId());
        assertEquals(1, price.getBrandId());
        assertEquals("2020-06-15-16.00.00", FormatUtil.toFormat(price.getStartDate()));
        assertEquals("2020-12-31-23.59.59", FormatUtil.toFormat(price.getEndDate()));
        assertEquals(4, price.getPriceList());
        assertEquals("38.95 EUR", price.getFinalProduct());

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200615_100000_then_price_30_50(){

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-15-10.00.00"))
                .productId(35455)
                .brandId(1)
                .build();

        Price price = useCase.execute(request);

        assertEquals(35455, price.getProductId());
        assertEquals(1, price.getBrandId());
        assertEquals("2020-06-15-00.00.00", FormatUtil.toFormat(price.getStartDate()));
        assertEquals("2020-06-15-11.00.00", FormatUtil.toFormat(price.getEndDate()));
        assertEquals(3, price.getPriceList());
        assertEquals("30.50 EUR", price.getFinalProduct());

    }

    @Test
    public void when_productId_35436_brandId_1_date_20200614_210000_then_Price_Not_Found_Exception() {

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-14-21.00.00"))
                .productId(35456)
                .brandId(1)
                .build();

        assertThrowsExactly(PriceNotFoundException.class, () -> {useCase.execute(request);;});

    }

    @Test
    public void when_productId_35435_brandId_2_date_20200614_210000_then_Price_Not_Found_Exception() {

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-14-21.00.00"))
                .productId(35455)
                .brandId(2)
                .build();

        assertThrowsExactly(PriceNotFoundException.class, () -> {useCase.execute(request);;});

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200612_210000_then_Price_Not_Found_Exception() {

        Mockito.when(priceRepository.findAll()).thenReturn( this.getPrices());

        SearchPriceRequest request = SearchPriceRequest.builder()
                .date(FormatUtil.dateParse("2020-06-12-21.00.00"))
                .productId(35455)
                .brandId(1)
                .build();

        assertThrowsExactly(PriceNotFoundException.class, () -> {useCase.execute(request);;});

    }

    private List<Price> getPrices() {

        List<Price> listPrices = new ArrayList<>();

        listPrices.add(Price.builder().brandId(1).startDate(FormatUtil.dateParse("2020-06-14-00.00.00"))
                .endDate(FormatUtil.dateParse("2020-12-31-23.59.59"))
                .priceList(1).productId((long) 35455).priority(0).price(new Amount(35.50))
                .currency(Currency.valueOf("EUR")).build());

        listPrices.add(Price.builder().brandId(1).startDate(FormatUtil.dateParse("2020-06-14-15.00.00"))
                .endDate(FormatUtil.dateParse("2020-06-14-18.30.00"))
                .priceList(2).productId((long) 35455).priority(1).price(new Amount(25.45))
                .currency(Currency.valueOf("EUR")).build());

        listPrices.add(Price.builder().brandId(1).startDate(FormatUtil.dateParse("2020-06-15-00.00.00"))
                .endDate(FormatUtil.dateParse("2020-06-15-11.00.00"))
                .priceList(3).productId((long) 35455).priority(1).price(new Amount(30.50))
                .currency(Currency.valueOf("EUR")).build());

        listPrices.add(Price.builder().brandId(1).startDate(FormatUtil.dateParse("2020-06-15-16.00.00"))
                .endDate(FormatUtil.dateParse("2020-12-31-23.59.59"))
                .priceList(4).productId((long) 35455).priority(1).price(new Amount(38.95))
                .currency(Currency.valueOf("EUR")).build());

        return listPrices;

    }

}