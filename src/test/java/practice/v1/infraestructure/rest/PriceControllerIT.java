package practice.v1.infraestructure.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import practice.v1.Application;
import practice.v1.infraestructure.rest.model.SearchPriceResponse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)

class PriceControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void when_productId_35435_brandId_1_date_20200614_100000_then_price_35_50() {

        ResponseEntity<SearchPriceResponse> responseEntity = restTemplate.getForEntity(
                "/prices?date=2020-06-14-10.00.00&productId=35455&brandId=1",
                SearchPriceResponse.class);

        assertTrue(responseEntity.getStatusCode().is2xxSuccessful());

        SearchPriceResponse response = responseEntity.getBody();

        assertEquals(35455, response.getProductId());
        assertEquals(1, response.getBrandId());
        assertEquals("2020-06-14-00.00.00", response.getStartDate());
        assertEquals("2020-12-31-23.59.59", response.getEndDate());
        assertEquals(1, response.getPriceList());
        assertEquals("35.50 EUR", response.getPrice());

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200614_160000_then_price_25_45() {

        ResponseEntity<SearchPriceResponse> responseEntity = restTemplate.getForEntity(
                "/prices?date=2020-06-14-16.00.00&productId=35455&brandId=1",
                SearchPriceResponse.class);

        assertTrue(responseEntity.getStatusCode().is2xxSuccessful());

        SearchPriceResponse response = responseEntity.getBody();

        assertEquals(35455, response.getProductId());
        assertEquals(1, response.getBrandId());
        assertEquals("2020-06-14-15.00.00", response.getStartDate());
        assertEquals("2020-06-14-18.30.00", response.getEndDate());
        assertEquals(2, response.getPriceList());
        assertEquals("25.45 EUR", response.getPrice());

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200614_210000_then_price_35_50() {

        ResponseEntity<SearchPriceResponse> responseEntity = restTemplate.getForEntity(
                "/prices?date=2020-06-14-21.00.00&productId=35455&brandId=1",
                SearchPriceResponse.class);

        assertTrue(responseEntity.getStatusCode().is2xxSuccessful());

        SearchPriceResponse response = responseEntity.getBody();

        assertEquals(35455, response.getProductId());
        assertEquals(1, response.getBrandId());
        assertEquals("2020-06-14-00.00.00", response.getStartDate());
        assertEquals("2020-12-31-23.59.59", response.getEndDate());
        assertEquals(1, response.getPriceList());
        assertEquals("35.50 EUR", response.getPrice());

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200615_100000_then_price_30_50() {

        ResponseEntity<SearchPriceResponse> responseEntity = restTemplate.getForEntity(
                "/prices?date=2020-06-15-10.00.00&productId=35455&brandId=1",
                SearchPriceResponse.class);

        assertTrue(responseEntity.getStatusCode().is2xxSuccessful());

        SearchPriceResponse response = responseEntity.getBody();

        assertEquals(35455, response.getProductId());
        assertEquals(1, response.getBrandId());
        assertEquals("2020-06-15-00.00.00", response.getStartDate());
        assertEquals("2020-06-15-11.00.00", response.getEndDate());
        assertEquals(3, response.getPriceList());
        assertEquals("30.50 EUR", response.getPrice());

    }

    @Test
    public void when_productId_35435_brandId_1_date_20200615_210000_then_price_38_95() {

        ResponseEntity<SearchPriceResponse> responseEntity = restTemplate.getForEntity(
                    "/prices?date=2020-06-15-21.00.00&productId=35455&brandId=1",
                SearchPriceResponse.class);

        assertTrue(responseEntity.getStatusCode().is2xxSuccessful());

        SearchPriceResponse response = responseEntity.getBody();

        assertEquals(35455, response.getProductId());
        assertEquals(1, response.getBrandId());
        assertEquals("2020-06-15-16.00.00", response.getStartDate());
        assertEquals("2020-12-31-23.59.59", response.getEndDate());
        assertEquals(4, response.getPriceList());
        assertEquals("38.95 EUR", response.getPrice());
    }

    @Test
    public void shouldNotWorkBadLocalDateTimeFormat(){

        ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity(
                "/prices?date=2020-06-15-21.00.60&productId=35455&brandId=1",
                SearchPriceResponse.class);

        assertTrue(res.getStatusCode().is4xxClientError());
        assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());


    }

    @Test
    public void shouldNotWorkBadBrand(){

        ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity(
                "/prices?date=2020-06-15-21.00.00&productId=35455&brandId=2",
                SearchPriceResponse.class);

        assertTrue(res.getStatusCode().is4xxClientError());
        assertEquals(HttpStatus.NOT_FOUND, res.getStatusCode());


    }

    @Test
    public void shouldNotWorkMissingParameter(){

        ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity(
                "/prices?date=2020-06-15-21.00.00&productId=35455",
                SearchPriceResponse.class);

        assertTrue(res.getStatusCode().is4xxClientError());
        assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());

    }

    @Test
    public void shouldNotWorkBadParameterIncluded(){

        ResponseEntity<SearchPriceResponse> res = restTemplate.getForEntity(
                "/prices?date=2020-06-15-21.00.00&productId=35455&brandId=a",
                SearchPriceResponse.class);

        assertTrue(res.getStatusCode().is4xxClientError());
        assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());

    }


}