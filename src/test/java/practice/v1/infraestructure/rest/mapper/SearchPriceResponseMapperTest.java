package practice.v1.infraestructure.rest.mapper;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import practice.v1.application.model.SearchPriceRequest;
import practice.v1.domain.aggregates.Price;
import practice.v1.domain.repositories.PriceRepository;
import practice.v1.domain.util.FormatUtil;
import practice.v1.domain.vos.Amount;
import practice.v1.domain.vos.Currency;
import practice.v1.infraestructure.rest.model.SearchPriceResponse;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SearchPriceResponseMapperTest {


    @Test
    public void mapperShouldTransformToSearchPriceResponse() {

        Price price = Price.builder()
                .startDate(FormatUtil.dateParse("2020-06-14-00.00.00"))
                .endDate(FormatUtil.dateParse("2020-12-31-23.59.59"))
                .productId(35455L)
                .priceList(1)
                .brandId(1)
                .price(new Amount(35.50))
                .currency(Currency.valueOf("EUR"))
                .build();

        SearchPriceResponse response = SearchPriceResponseMapper.toResponse(price);

        assertEquals(35455, response.getProductId());
        assertEquals(1, response.getBrandId());
        assertEquals("2020-06-14-00.00.00", response.getStartDate());
        assertEquals("2020-12-31-23.59.59", response.getEndDate());
        assertEquals(1, response.getPriceList());
        assertEquals("35.50 EUR", response.getPrice());


    }

}