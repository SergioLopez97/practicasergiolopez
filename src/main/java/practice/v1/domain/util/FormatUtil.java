package practice.v1.domain.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class FormatUtil {

    private FormatUtil() {}

    static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss");

    public static LocalDateTime dateParse(String date){

        return LocalDateTime.parse(date, dtf);

    }

    public static String toFormat (LocalDateTime date){

        return date.format(dtf);

    }

    public static String formatDouble (Double num){

        return String.format(Locale.US,"%.2f", num);

    }
    
}
