package practice.v1.domain.vos;

public class Amount {

    private Double value;

    public Amount (Double amount){

        if (amount > 0){

            this.value = amount;

        }else{

        throw new IllegalArgumentException("Amount can't be negative");

        }

    }

    public Double getValue(){

        return value;

    }
    
}
