package practice.v1.domain.services;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

import org.springframework.stereotype.Service;

import practice.v1.domain.aggregates.Price;
import practice.v1.domain.exceptions.PriceNotFoundException;
import practice.v1.domain.repositories.PriceRepository;

@Service
public class PriceService {

    private PriceRepository priceRepository;

    public PriceService(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    public Price searchPrice (LocalDateTime date, Long productId, Integer brandId) {

        return priceRepository.findAll().stream()
        .filter(p -> !date.isBefore(p.getStartDate()) && !date.isAfter(p.getEndDate()))
        .filter(p -> p.getProductId().equals(productId))
        .filter(p -> p.getBrandId().equals(brandId))
        .max(Comparator.comparingInt(Price::getPriority))
        .orElseThrow(() -> new PriceNotFoundException("Product not found"));

    }
    
}
