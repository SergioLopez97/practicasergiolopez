package practice.v1.domain.repositories;

import java.util.List;

import practice.v1.domain.aggregates.Price;

public interface PriceRepository {

    public List<Price> findAll();
    
}
