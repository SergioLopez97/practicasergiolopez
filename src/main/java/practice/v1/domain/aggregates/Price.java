package practice.v1.domain.aggregates;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;
import practice.v1.domain.util.FormatUtil;
import practice.v1.domain.vos.Amount;
import practice.v1.domain.vos.Currency;

@Data
@Builder
public class Price {
    
    private Integer brandId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Integer priceList;
    private Long productId;
    private Integer priority;
    private Amount price;
    private Currency currency;

    public String getFinalProduct(){

      return FormatUtil.formatDouble(this.price.getValue()) + " " + this.currency.getValue();

    }

}
