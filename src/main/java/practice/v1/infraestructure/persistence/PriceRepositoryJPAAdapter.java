package practice.v1.infraestructure.persistence;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import practice.v1.infraestructure.mappers.PriceMapper;
import practice.v1.domain.aggregates.Price;
import practice.v1.domain.repositories.PriceRepository;

@Repository
public class PriceRepositoryJPAAdapter implements PriceRepository {

    @Autowired
    private PriceRepositoryJPA priceRepositoryJPA;

    @Override
    public List<Price> findAll() {

        return this.priceRepositoryJPA.findAll()
        .stream().map(PriceMapper:: toModel).collect(Collectors.toList());

    }


    
}
