package practice.v1.infraestructure.rest.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchPriceResponse {
        
    private final long productId;
    private final int brandId;
    private final int priceList; 
    private final String startDate;
    private final String endDate;  
    private final String price;


}
