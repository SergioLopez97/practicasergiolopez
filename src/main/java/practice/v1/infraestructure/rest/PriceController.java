package practice.v1.infraestructure.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import practice.v1.application.model.SearchPriceRequest;
import practice.v1.domain.aggregates.Price;
import practice.v1.infraestructure.rest.mapper.SearchPriceResponseMapper;
import practice.v1.infraestructure.rest.model.SearchPriceResponse;
import practice.v1.application.usecases.SearchPriceUseCase;
import practice.v1.domain.util.FormatUtil;

@RestController
public class PriceController {

    @Autowired
    private SearchPriceUseCase useCase;
    
    @GetMapping("/prices")
    public ResponseEntity<SearchPriceResponse> searchPrice(@RequestParam(required = true) String date,
                                                             @RequestParam(required = true) long productId, @RequestParam(required = true) int brandId){

        	SearchPriceRequest request = SearchPriceRequest.builder()
            .date(FormatUtil.dateParse(date))
            .productId(productId)
            .brandId(brandId)
            .build();

            Price price = useCase.execute(request);
            SearchPriceResponse response = SearchPriceResponseMapper.toResponse(price);

                return ResponseEntity.status(HttpStatus.OK).body(response);


    }

}
