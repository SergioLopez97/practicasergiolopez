package practice.v1.infraestructure.rest.mapper;

import practice.v1.domain.aggregates.Price;
import practice.v1.domain.util.FormatUtil;
import practice.v1.infraestructure.rest.model.SearchPriceResponse;

public class SearchPriceResponseMapper {

    public static SearchPriceResponse toResponse(Price price) {
        return SearchPriceResponse.builder()
                .productId(price.getProductId()).brandId(price.getBrandId())
                .startDate(FormatUtil.toFormat(price.getStartDate()))
                .endDate(FormatUtil.toFormat(price.getEndDate()))
                .price(price.getFinalProduct())
                .priceList(price.getPriceList())
                .build();
    }

}
