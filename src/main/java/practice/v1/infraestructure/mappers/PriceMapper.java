package practice.v1.infraestructure.mappers;

import practice.v1.domain.aggregates.Price;
import practice.v1.domain.vos.Amount;
import practice.v1.domain.vos.Currency;
import practice.v1.infraestructure.persistence.PriceJPA;

public class PriceMapper {

    private PriceMapper(){
            
    }

    public static Price toModel (PriceJPA price){

        return Price.builder()
        .price(new Amount(price.getPrice()))
        .brandId(price.getBrandId())
        .currency(Currency.valueOf(price.getCurrency()))
        .endDate(price.getEndDate())
        .startDate(price.getStartDate())
        .priceList(price.getPriceList())
        .priority(price.getPriority())
        .productId(price.getProductId())
        .build();

    }
    
}
