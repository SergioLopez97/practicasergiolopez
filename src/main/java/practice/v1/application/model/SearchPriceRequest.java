package practice.v1.application.model;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SearchPriceRequest {

    private final LocalDateTime date;
    private final long productId;
    private final int brandId;
    
    
}
