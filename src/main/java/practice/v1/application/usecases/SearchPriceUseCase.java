package practice.v1.application.usecases;

import org.springframework.stereotype.Service;

import practice.v1.application.model.SearchPriceRequest;
import practice.v1.domain.aggregates.Price;
import practice.v1.domain.services.PriceService;
import practice.v1.domain.util.FormatUtil;

@Service
public class SearchPriceUseCase {

    private PriceService priceService;

    public SearchPriceUseCase(PriceService priceService) {
        this.priceService = priceService;
    }

    public Price execute (SearchPriceRequest request) {

        return priceService.searchPrice(request.getDate(), request.getProductId(), request.getBrandId());


    }
    
}
